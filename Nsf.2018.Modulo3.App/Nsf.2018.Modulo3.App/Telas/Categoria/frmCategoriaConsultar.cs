﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmCategoriaConsultar : UserControl
    {
        public frmCategoriaConsultar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CategoriaBusiness business = new CategoriaBusiness();
                List<CategoriaDTO> lista = business.Consultar(txtCategoria.Text.Trim());

                dgvCategorias.AutoGenerateColumns = false;
                dgvCategorias.DataSource = lista;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvCategorias_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                CategoriaDTO cat = dgvCategorias.CurrentRow.DataBoundItem as CategoriaDTO;
                
                CategoriaBusiness business = new CategoriaBusiness();
                business.Remover(cat.Id);

                button1_Click(null, null);
            }
        }
    }
}
