﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;
using Nsf._2018.Modulo3.App.Plugin;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmCategoriaCadastrar : UserControl
    {
        public frmCategoriaCadastrar()
        {
            InitializeComponent();
        }


        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                CategoriaDTO dto = new CategoriaDTO();
                dto.Nome = txtCategoria.Text.Trim();

                CategoriaBusiness business = new CategoriaBusiness();
                business.Salvar(dto);

                MessageBox.Show("Categoria salva com sucesso.", "DBZ",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
    }
}
