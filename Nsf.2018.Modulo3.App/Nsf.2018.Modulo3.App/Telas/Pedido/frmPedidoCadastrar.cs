﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;
using Nsf._2018.Modulo3.App.DB.Pedido;
using Nsf._2018.Modulo3.App.Plugin;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoCadastrar : UserControl
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();
        BindingList<ClienteDTO> clientes = new BindingList<ClienteDTO>();


        public frmPedidoCadastrar()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }

        
        void CarregarCombos()
        {
            cboFormaPag.SelectedIndex = 0;
            
            // Combo Categoria
            CategoriaBusiness catBusiness = new CategoriaBusiness();
            List<CategoriaDTO> categorias = catBusiness.Listar();

            cboCategoria.ValueMember = nameof(CategoriaDTO.Id);
            cboCategoria.DisplayMember = nameof(CategoriaDTO.Nome);
            cboCategoria.DataSource = categorias;


            // Combo Clientes (Bindinglist)
            ClienteBusiness clienteBusiness = new ClienteBusiness();
            List<ClienteDTO> clienteLista = clienteBusiness.Listar();
            clientes = new BindingList<ClienteDTO>(clienteLista);

            cboCliente.ValueMember = nameof(ClienteDTO.Id);
            cboCliente.DisplayMember = nameof(ClienteDTO.Nome);
            cboCliente.DataSource = clientes;
        }



        void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }

        private void btnEmitir_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO cliente = cboCliente.SelectedItem as ClienteDTO;

                PedidoDTO dto = new PedidoDTO();
                dto.FuncionarioId = UserSession.UsuarioLogado.Id;
                dto.ClienteId = cliente.Id;
                dto.FormaPagamento = cboFormaPag.Text;
                dto.Data = DateTime.Now;

                PedidoBusiness business = new PedidoBusiness();
                business.Salvar(dto, produtosCarrinho.ToList());

                MessageBox.Show("Pedido salvo com sucesso.", "DBZ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;

                int qtd = Convert.ToInt32(txtQuantidade.Text);

                for (int i = 0; i < qtd; i++)
                {
                    produtosCarrinho.Add(dto);
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;
                imgProduto.Image = ImagemPlugin.ConverterParaImagem(dto.Imagem);
            }
            catch
            {
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmClienteCadastrar tela = new frmClienteCadastrar();
            tela.ShowDialog();

            if (tela.ClienteCriado != null)
            {
                clientes.Add(tela.ClienteCriado);
                cboCliente.SelectedItem = tela.ClienteCriado;
            }
        }

        private void cboCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            CategoriaDTO categoria = cboCategoria.SelectedItem as CategoriaDTO;
            CarregarComboProdutos(categoria.Id);
        }


        private void CarregarComboProdutos(int categoriaId)
        {
            // Combo Produtos
            ProdutoBusiness prodBusiness = new ProdutoBusiness();
            List<ProdutoDTO> produtos = prodBusiness.Consultar(categoriaId);

            cboProduto.ValueMember = nameof(ProdutoDTO.Id);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
            cboProduto.DataSource = produtos;

        }
    }
}
