﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Pedido;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoConsultar : UserControl
    {
        public frmPedidoConsultar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PedidoBusiness business = new PedidoBusiness();
                List<PedidoConsultarView> lista = business.Consultar(txtCliente.Text.Trim());

                dgvPedidos.AutoGenerateColumns = false;
                dgvPedidos.DataSource = lista;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvPedidos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 5)
                {
                    PedidoConsultarView pedido = dgvPedidos.Rows[e.RowIndex].DataBoundItem as PedidoConsultarView;

                    DialogResult r = MessageBox.Show($"Deseja realmente excluir o pedido {pedido.Id}?", "DBZ", 
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        PedidoBusiness business = new PedidoBusiness();
                        business.Remover(pedido.Id);

                        button1_Click(null, null);
                    }
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
