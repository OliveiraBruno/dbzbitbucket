﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoConsultar : UserControl
    {
        public frmProdutoConsultar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoBusiness business = new ProdutoBusiness();
                List<ProdutoConsultarView> lista = business.Consultar(txtProduto.Text.Trim());

                dgvProdutos.AutoGenerateColumns = false;
                dgvProdutos.DataSource = lista;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvProdutos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 4)
                {
                    ProdutoConsultarView produto = dgvProdutos.Rows[e.RowIndex].DataBoundItem as ProdutoConsultarView;

                    DialogResult r = MessageBox.Show($"Deseja realmente excluir o produto {produto.Id}?", "DBZ",
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        ProdutoBusiness business = new ProdutoBusiness();
                        business.Remover(produto.Id);

                        button1_Click(null, null);
                    }
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
