﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;
using Nsf._2018.Modulo3.App.Plugin;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoCadastrar : UserControl
    {
        public frmProdutoCadastrar()
        {
            InitializeComponent();
            CarregarCombos();
        }

        void CarregarCombos()
        {
            CategoriaBusiness bus = new CategoriaBusiness();
            List<CategoriaDTO> lista = bus.Listar();

            cboCategoria.ValueMember = nameof(CategoriaDTO.Id);
            cboCategoria.DisplayMember = nameof(CategoriaDTO.Nome);
            cboCategoria.DataSource = lista;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                CategoriaDTO categoria = cboCategoria.SelectedItem as CategoriaDTO;

                ProdutoDTO dto = new ProdutoDTO();
                dto.Nome = txtProduto.Text.Trim();
                dto.Preco = Convert.ToDecimal(txtPreco.Text.Trim());
                dto.Imagem = ImagemPlugin.ConverterParaString(imgProduto.Image);
                dto.CategoriaId = categoria.Id;

                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Produto salvo com sucesso.", "DBZ",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        

        private void imgProduto_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgProduto.ImageLocation = dialog.FileName;
            }
        }
    }
}
