﻿using Nsf._2018.Modulo3.App.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmClienteCadastrar : Form
    {
        public ClienteDTO ClienteCriado { get; set; }

        public frmClienteCadastrar()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO cliente = new ClienteDTO();
                cliente.Nome = txtCliente.Text;
                cliente.Cpf = txtCpf.Text;

                ClienteBusiness business = new ClienteBusiness();
                business.Salvar(cliente);

                MessageBox.Show("Cliente Salvo com sucesso.", "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);


                this.ClienteCriado = cliente;
                this.Close();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "DBZ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
