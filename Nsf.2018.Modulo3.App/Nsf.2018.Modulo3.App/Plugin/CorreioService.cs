﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.Plugin
{
    class CorreioService
    {
        public CorreioEnderecoModel Consultar(string cep)
        {
            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;

            string result = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json/");
            dynamic correio = JsonConvert.DeserializeObject(result);

            if (correio.erro != null && correio.erro == true)
                throw new ArgumentException("CEP inválido");

            CorreioEnderecoModel endereco = new CorreioEnderecoModel();
            endereco.Cep = correio.cep;
            endereco.Endereco = correio.logradouro + ", " + correio.complemento;
            endereco.Cidade = correio.localidade;
            endereco.Estado = correio.uf;
            endereco.Bairro = correio.bairro;

            return endereco;
        }
    }


    class CorreioEnderecoModel
    {
        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
    }


}
