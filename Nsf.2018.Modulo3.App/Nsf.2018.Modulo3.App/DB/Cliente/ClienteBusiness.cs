﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO dto)
        {
            ClienteDTO cliente = this.ConsultarPorCpf(dto.Cpf);
            if (cliente != null)
            {
                throw new ArgumentException("CPF já cadastrado no sistema.");
            }


            ClienteDatabase db = new ClienteDatabase();
            return db.Salvar(dto);
        }

        public ClienteDTO ConsultarPorCpf(string cpf)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.ConsultarPorCpf(cpf);
        }

        public List<ClienteDTO> Listar()
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Listar();
        }

    }
}
