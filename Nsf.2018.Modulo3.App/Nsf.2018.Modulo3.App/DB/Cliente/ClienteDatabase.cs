﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @"INSERT INTO tb_cliente (nm_cliente, ds_cpf) 
                                   VALUES (@nm_cliente, @ds_cpf)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public ClienteDTO ConsultarPorCpf(string cpf)
        {
            string script = @"SELECT * FROM tb_cliente WHERE ds_cpf = @ds_cpf";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_cpf", cpf));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            ClienteDTO dto = null;
            if (reader.Read())
            {
                dto = new ClienteDTO();

                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Cpf = reader.GetString("ds_cpf");
            }
            reader.Close();

            return dto;
        }


        public List<ClienteDTO> Listar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Cpf = reader.GetString("ds_cpf");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}
