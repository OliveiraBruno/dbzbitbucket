﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class CategoriaDatabase
    {
        public int Salvar(CategoriaDTO dto)
        {
            string script = @"INSERT INTO tb_categoria (nm_categoria) 
                                   VALUES (@nm_categoria)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_categoria", dto.Nome));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_categoria WHERE id_categoria = @id_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_categoria", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<CategoriaDTO> Listar()
        {
            string script = @"SELECT * FROM tb_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CategoriaDTO> lista = new List<CategoriaDTO>();
            while (reader.Read())
            {
                CategoriaDTO dto = new CategoriaDTO();
                dto.Id = reader.GetInt32("id_categoria");
                dto.Nome = reader.GetString("nm_categoria");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public List<CategoriaDTO> Consultar(string nome)
        {
            string script = @"SELECT * FROM tb_categoria WHERE nm_categoria LIKE @nm_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_categoria", "%" + nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CategoriaDTO> lista = new List<CategoriaDTO>();
            while (reader.Read())
            {
                CategoriaDTO dto = new CategoriaDTO();
                dto.Id = reader.GetInt32("id_categoria");
                dto.Nome = reader.GetString("nm_categoria");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public CategoriaDTO ConsultarPorNome(string nome)
        {
            string script = @"SELECT * FROM tb_categoria WHERE nm_categoria = @nm_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_categoria", nome));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            CategoriaDTO dto = null;
            while (reader.Read())
            {
                dto = new CategoriaDTO();
                dto.Id = reader.GetInt32("id_categoria");
                dto.Nome = reader.GetString("nm_categoria");
            }
            reader.Close();

            return dto;
        }

    }
}
