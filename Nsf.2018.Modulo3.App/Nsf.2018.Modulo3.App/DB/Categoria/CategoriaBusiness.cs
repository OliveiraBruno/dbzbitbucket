﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class CategoriaBusiness
    {
        public int Salvar(CategoriaDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            CategoriaDTO cat = this.ConsultarPorNome(dto.Nome);
            if (cat != null)
            {
                throw new ArgumentException("Categoria já existe no sistema.");
            }

            CategoriaDatabase db = new CategoriaDatabase();
            return db.Salvar(dto);
        }


        public void Remover(int id)
        {
            CategoriaDatabase db = new CategoriaDatabase();
            db.Remover(id);
        }


        public CategoriaDTO ConsultarPorNome(string nome)
        {
            CategoriaDatabase db = new CategoriaDatabase();
            return db.ConsultarPorNome(nome);
        }


        public List<CategoriaDTO> Consultar(string nome)
        {
            CategoriaDatabase db = new CategoriaDatabase();
            return db.Consultar(nome);
        }


        public List<CategoriaDTO> Listar()
        {
            CategoriaDatabase db = new CategoriaDatabase();
            return db.Listar();
        }

    }
}
