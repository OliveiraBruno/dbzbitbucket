﻿using Nsf._2018.Modulo3.App.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<ProdutoDTO> produtos)
        {
            if (pedido.FormaPagamento == "Selecione")
            {
                throw new ArgumentException("Forma de Pagamento é obrigatório.");
            }

            if (pedido.ClienteId == 0)
            {
                throw new ArgumentException("Cliente é obrigatório.");
            }

            if (pedido.FuncionarioId == 0)
            {
                throw new ArgumentException("Funcionário é obrigatório.");
            }

            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            PedidoItemBusiness itemBusiness = new PedidoItemBusiness();
            foreach (ProdutoDTO item in produtos)
            {
                PedidoItemDTO itemDto = new PedidoItemDTO();
                itemDto.IdPedido = idPedido;
                itemDto.IdProduto = item.Id;

                itemBusiness.Salvar(itemDto);
            }

            return idPedido;
        }

        public void Remover(int pedidoId)
        {
            PedidoItemBusiness itemBusiness = new PedidoItemBusiness();
            List<PedidoItemDTO> itens = itemBusiness.ConsultarPorPedido(pedidoId);

            foreach (PedidoItemDTO item in itens)
            {
                itemBusiness.Remover(item.Id);
            }

            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            pedidoDatabase.Remover(pedidoId);
        }

        public List<PedidoConsultarView> Consultar(string cliente)
        {
            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            return pedidoDatabase.Consultar(cliente);
        }


    }

}
