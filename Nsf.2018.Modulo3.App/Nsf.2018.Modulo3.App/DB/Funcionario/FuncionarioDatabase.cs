﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class FuncionarioDatabase
    {
        public FuncionarioDTO Logar(string login, string senha)
        {
            string script = @"SELECT * FROM tb_funcionario WHERE ds_login = @ds_login AND ds_senha = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_login", login));
            parms.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO funcionario = null;

            if (reader.Read())
            {
                funcionario = new FuncionarioDTO();
                funcionario.Id = reader.GetInt32("id_funcionario");
                funcionario.Nome = reader.GetString("nm_funcionario");
                funcionario.Login = reader.GetString("ds_login");
                funcionario.PermissaoAdm = reader.GetBoolean("bt_permissao_adm");
                funcionario.PermissaoProduto = reader.GetBoolean("bt_permissao_produto");
                funcionario.PermissaoPedido = reader.GetBoolean("bt_permissao_pedido");
            }
            reader.Close();

            return funcionario;
        }

    }
}
