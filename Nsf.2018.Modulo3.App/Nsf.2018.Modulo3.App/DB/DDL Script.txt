﻿drop database DBZStoreDB;
create database DBZStoreDB;

use DBZStoreDB;



CREATE TABLE `tb_produto` (
	`id_produto` INT NOT NULL AUTO_INCREMENT,
	`nm_produto` varchar(100) NOT NULL,
	`vl_preco` DECIMAL(15,2) NOT NULL,
	`img_produto` longblob NOT NULL,
	`id_categoria` INT NOT NULL,
	PRIMARY KEY (`id_produto`)
);

CREATE TABLE `tb_pedido` (
	`id_pedido` INT NOT NULL AUTO_INCREMENT,
	`id_cliente` INT NOT NULL,
	`id_funcionario` INT NOT NULL,
	`dt_venda` DATE NOT NULL,
	`ds_forma_pag` varchar(10) NOT NULL,
	PRIMARY KEY (`id_pedido`)
);

CREATE TABLE `tb_pedido_item` (
	`id_pedido_item` INT NOT NULL AUTO_INCREMENT,
	`id_pedido` INT NOT NULL,
	`id_produto` INT NOT NULL,
	PRIMARY KEY (`id_pedido_item`)
);

CREATE TABLE `tb_funcionario` (
	`id_funcionario` INT NOT NULL AUTO_INCREMENT,
	`nm_funcionario` varchar(100) NOT NULL,
	`ds_login` varchar(100) NOT NULL,
	`ds_senha` varchar(400) NOT NULL,
	`bt_permissao_adm` BOOLEAN NOT NULL,
	`bt_permissao_produto` BOOLEAN NOT NULL,
	`bt_permissao_pedido` BOOLEAN NOT NULL,
	PRIMARY KEY (`id_funcionario`)
);

CREATE TABLE `tb_categoria` (
	`id_categoria` INT NOT NULL AUTO_INCREMENT,
	`nm_categoria` varchar(100) NOT NULL,
	PRIMARY KEY (`id_categoria`)
);

CREATE TABLE `tb_cliente` (
	`id_cliente` INT NOT NULL AUTO_INCREMENT,
	`nm_cliente` varchar(100) NOT NULL,
	`ds_cpf` varchar(100) NOT NULL,
	PRIMARY KEY (`id_cliente`)
);

ALTER TABLE `tb_produto` ADD CONSTRAINT `tb_produto_fk0` FOREIGN KEY (`id_categoria`) REFERENCES `tb_categoria`(`id_categoria`);

ALTER TABLE `tb_pedido` ADD CONSTRAINT `tb_pedido_fk0` FOREIGN KEY (`id_cliente`) REFERENCES `tb_cliente`(`id_cliente`);

ALTER TABLE `tb_pedido` ADD CONSTRAINT `tb_pedido_fk1` FOREIGN KEY (`id_funcionario`) REFERENCES `tb_funcionario`(`id_funcionario`);

ALTER TABLE `tb_pedido_item` ADD CONSTRAINT `tb_pedido_item_fk0` FOREIGN KEY (`id_pedido`) REFERENCES `tb_pedido`(`id_pedido`);

ALTER TABLE `tb_pedido_item` ADD CONSTRAINT `tb_pedido_item_fk1` FOREIGN KEY (`id_produto`) REFERENCES `tb_produto`(`id_produto`);



CREATE VIEW vw_pedido_consultar AS 
	SELECT tb_pedido.id_pedido,
		   tb_cliente.nm_cliente,
           tb_pedido.dt_venda,
		   count(tb_pedido_item.id_pedido_item) 	as qtd_itens,
           sum(tb_produto.vl_preco)		 			as vl_total
      FROM tb_pedido
      JOIN tb_pedido_item
        ON tb_pedido.id_pedido = tb_pedido_item.id_pedido
	  JOIN tb_produto
        ON tb_pedido_item.id_produto = tb_produto.id_produto
	  JOIN tb_cliente
        ON tb_cliente.id_cliente = tb_pedido.id_cliente
	 GROUP 
	    BY tb_pedido.id_pedido,
		   tb_cliente.nm_cliente,
           tb_pedido.dt_venda;


Create VIEW VW_PRODUTO_CONSULTA AS
	select tb_produto.id_produto,
		   tb_produto.nm_produto,
           tb_produto.img_produto,
		   tb_produto.vl_preco,
		   tb_categoria.nm_categoria
	  from tb_produto
	  join tb_categoria
		on tb_produto.id_categoria = tb_categoria.id_categoria;
