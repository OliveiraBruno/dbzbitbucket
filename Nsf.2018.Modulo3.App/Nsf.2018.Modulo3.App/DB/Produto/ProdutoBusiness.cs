﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (dto.Preco <= 0)
            {
                throw new ArgumentException("Preço é obrigatório.");
            }

            if (dto.CategoriaId == 0)
            {
                throw new ArgumentException("Categoria é obrigatório.");
            }

            ProdutoDatabase db = new ProdutoDatabase();

            List<ProdutoConsultarView> produtos = db.Consultar(dto.Nome);
            if (produtos.Count > 0)
            {
                throw new ArgumentException("Produto com mesmo nome já cadastrado.");
            }

            return db.Salvar(dto);
        }

        public void Alterar(ProdutoDTO dto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            db.Remover(id);
        }

        public List<ProdutoConsultarView> Consultar(string produto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Consultar(produto);
        }

        public List<ProdutoDTO> Consultar(int id)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Consultar(id);
        }


        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }

        


    }
}
